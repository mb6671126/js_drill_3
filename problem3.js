// Find all users with masters Degree.

function userWithMasterDegree(userData){
    try{
        if(userData.length === 0){
            throw new Error("User Data is Empty");
        }
    const result = [];
    for(let user in userData){
        if(userData[user].qualification.includes("Masters")){
            result.push(userData[user]);
        }
    }
    return result;
}
catch(error){
    console.error("Error in userWithMasterDegree function",error.message);       
}
}

module.exports = userWithMasterDegree;