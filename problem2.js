//  Q2 Find all users staying in Germany.

function userStayingInGermany(userData){
  try{
    if(userData.length === 0){
        throw new Error("User Data is Empty");
    }
    const result = [];
    for(let user in userData){
        if(userData[user].nationality === "Germany"){
            result.push(userData[user]);
        }
    }
    return result;
  }

  catch(error){
    console.error("Error in userStayingInGermany function", error.message);
  }  
}

module.exports = userStayingInGermany;