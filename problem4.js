//   Q4 Group users based on their Programming language mentioned in their designation.

function groupUsersOnProgramingL(userData){
    try{
        if(userData.length === 0){
            throw new Error("User Data is Empty");
        }
    let developerslanguage = {pythonDev: [], javascriptDev: [], golangDev: []};
    let pythonDev = [];
    let javascriptDev = [];
    let golangDev = [];
    
    for(let user in userData){
        if(userData[user].designation.toLowerCase().includes("python")){
            pythonDev.push(userData[user]);
        }
        if(userData[user].designation.toLowerCase().includes("javascript")){
            javascriptDev.push(userData[user]);
        }
        if(userData[user].designation.toLowerCase().includes("golang")){
            golangDev.push(userData[user]);
        }
    }
    developerslanguage.pythonDev = pythonDev;
    developerslanguage.javascriptDev = javascriptDev;
    developerslanguage.golangDev = golangDev;

    return developerslanguage;

}
catch(error){
    console.error("Error in groupUsersOnProgramingL function", error.message);
}
}

module.exports = groupUsersOnProgramingL;