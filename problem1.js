//  Q1 Find all users who are interested in playing video games.


function userPlayVideoGames(userData){
try{    
    if(userData.length === 0){
        throw new Error("User data is Empty.");
    }
    const result = [];
    for(let user in userData){
            if(userData[user].interests[0].includes('Video Games')){
               result.push(userData[user]);
            }
    }
    return result;
}
catch(error){
    console.error("Error in userPlayVideoGames function", error.message);
}
}

module.exports = userPlayVideoGames;